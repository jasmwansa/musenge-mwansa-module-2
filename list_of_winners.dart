import './mtnAppClass.dart';

void main() {
  // List containing all the apps and their information.
  List<Map> _winning_apps = [
    {
      "Name": "FNB Banking",
      "Year": 2012,
      "Category": "Best iOS,Android & Blackberry Consumer App",
      "Developer": "The FNB Banking app team"
    },
    {
      "Name": "SnapScan",
      "Year": 2013,
      "Category": "Best HTML 5 App",
      "Developer": "Kobus Ehlers"
    },
    {
      "Name": "LIVE-inspect",
      "Year": 2014,
      "Category": "Best Enterprise App",
      "Developer": "Lightstone"
    },
    {
      "Name": "WumDrop",
      "Year": 2015,
      "Category": "Best Enterprise App",
      "Developer": "Simon Hartley & Roy Borole"
    },
    {
      "Name": "Domestly",
      "Year": 2016,
      "Category": "Best Consumer App",
      "Developer": "Berno Potgieter & Thatoyaona Marumo"
    },
    {
      "Name": "Shyft",
      "Year": 2017,
      "Category": "Best Financial Solution",
      "Developer": "Currencycloud & The Standard Bank App Team"
    },
    {
      "Name": "Khula",
      "Year": 2018,
      "Category": "Best Agriculture Solution",
      "Developer": "Karidas Tshintsholo, Matthew Piper & Jackson Dyora"
    },
    {
      "Name": "Naked",
      "Year": 2019,
      "Category": "Best Financial Solution & Best App Accolades",
      "Developer": "Sumarie Greybe, Ernest North & Alex Thomson"
    },
    {
      "Name": "EasyEquities",
      "Year": 2020,
      "Category": "Best Consumer Solution",
      "Developer": "Charles Savage"
    },
    {
      "Name": "Ambani Afrika",
      "Year": 2021,
      "Category":
          "Best Gaming & Educational Solution, Best South African Solution",
      "Developer": "Mukundi Lambani"
    }
  ];

  // Sorted the list using the name of the app in alphabetical.
  print("\nList of MTN Business Apps sorted in alphabetical order :\n");
  _winning_apps.sort((a, b) => (a['Name']).compareTo(b['Name']));
  _winning_apps.forEach((element) {
    print(element['Name']);
  });

  // Printed the winners of 2017 and 2018
  print("\nWinning App for 2017 :");
  print(_winning_apps[7]['Name']);

  print("\nWinning App for 2018 :");
  print(_winning_apps[4]['Name']);

  // Total number of apps
  print("\nTotal Number of Apps :");
  print(_winning_apps.length);

  // Used an object to print app details and Using a function to capitalize all the names.
  appOfTheYear applications = appOfTheYear(_winning_apps);
  applications.showApps();
  applications.printNamesCapitalized();
}
