// Answer to question 3.

class appOfTheYear {
  late List<Map> list_of_apps;

  appOfTheYear(list_of_apps) {
    this.list_of_apps = list_of_apps;
  }

  showApps() {
    print("\nApp Details :");
    list_of_apps.forEach((app) {
      print("\nName\t\t: ${app['Name']}");
      print("Year\t\t: ${app['Year']}");
      print("Category\t: MTN App of the year & ${app['Category']}");
      print("Developer\t: ${app['Developer']}");
    });
  }

  printNamesCapitalized() {
    print("\nCapitalized App Names :\n");
    list_of_apps.forEach((app) {
      print((app['Name']).toUpperCase());
    });
  }
}
