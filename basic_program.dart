import 'dart:io';

void main() {
  print('\n');
  stdout.write("Please enter your name : ");
  String? name = stdin.readLineSync();
  print('\n');

  stdout.write("Please enter your favorite app : ");
  String? fave_app = stdin.readLineSync();
  print('\n');

  stdout.write("Please enter your city : ");
  String? city = stdin.readLineSync();
  print('\n');

  print("Your name is $name");
  print("Your favorite app is $fave_app");
  print("The name of your city is $city");
}
